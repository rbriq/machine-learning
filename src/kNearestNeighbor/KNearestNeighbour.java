package kNearestNeighbor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class KNearestNeighbour {
	private static final int allDataSize = 1000;
	private static final int testDataSize = 5;
	private List<Integer> testDataIndices = new ArrayList<Integer>(testDataSize);
	private Map<String, String> attributesNameTypeMap = new HashMap<String, String>();
	private List<HashMap<String, String>> data = new ArrayList<HashMap<String, String>>();
	private int correctAnswer = 0;

	private int tp = 0;
	private int fn = 0;
	private int tn = 0;
	private int fp = 0;

	public static void main(String[] args) throws IOException {
		KNearestNeighbour knn = new KNearestNeighbour();
		// question 2 part b
		knn.generateRandom(testDataSize);
		knn.parseData("C:\\Users\\Noona\\Documents\\data_exercise_1.csv");
		/*
		 * knn.findKNearestNeighbour(1, testDataSize);
		 * knn.findKNearestNeighbour(2, testDataSize);
		 * knn.findKNearestNeighbour(3, testDataSize);
		 */
		// knn.generateRandom(1);
		// question 2 part c
		System.out.println("*****************1-cross-validation**************");
		for (int k = 1; k <= 3; k++) {
			knn.clearCorrectAnswers();
			for (int i = 0; i < allDataSize; i++) {
				knn.setTestDataIndex(i);
				knn.findKNearestNeighbour(k, 1);
			}
			System.out.println("k=" + k +": ");
			System.out.println("number of false positives fp: "
					+ knn.getFp());
			System.out.println("number of false negatives fn: "
					+ knn.getFn());
			System.out.println("number of true positives tp: "
					+ knn.getTp());
			System.out.println("number of true negatives tn: "
					+ knn.getTn());

			double precision = ((double) (knn.getTp()))
					/ (knn.getTp() + knn.getFp());
			System.out.println("precision: " + precision);

			double recall = ((double) (knn.getTp()))
					/ (knn.getTp() + knn.getFn());
			System.out.println("recall: " + recall);

			double fMeasure = (precision * recall) / (precision + recall) * 2;
			System.out.println("F-measure: " + fMeasure);

			double accurary = (((double) (knn.getTp())) + (knn.getTn()))
					/ (knn.getTp() + knn.getFn() + knn.getTn() + knn.getFp());
			System.out.println("Accuracy: " + accurary);
			System.out
					.println("******************************************************");

			/*System.out.println("Correct answers for k=" + k + ": "
					+ knn.getCorrectAnswers());

			System.out.println("accuracy answers for k=" + k + ": "
					+ ((double) knn.getCorrectAnswers()) / allDataSize);*/
		}

	}

	private void clearCorrectAnswers() {
		correctAnswer = 0;
		fn = 0;
		fp = 0;
		tn = 0;
		tp = 0;

	}

	public int getCorrectAnswers() {
		return correctAnswer;
	}

	private void setTestDataIndex(int i) {
		testDataIndices.clear();
		testDataIndices.add(i);
	}

	public int deltaCategorial(String a, String b) {
		if (a.equals(b))
			return 0;
		return 1;
	}

	public int deltaBinary(boolean a, boolean b) {
		if (a == b)
			return 0;
		return 1;
	}

	public double deltaNumerical(double a, double b) {
			// normalize the distance to a value between 0 and 1, so there's no
			// disrepancy from binary or categorical attributes.
			return (Math.abs(a - b))/(Math.abs(a)+Math.abs(b));
	}

	public boolean getBooleanValue(String string) {
		return string.equalsIgnoreCase("yes") ? true : false;
	}

	public void findKNearestNeighbour(int k, int testDataSize1) {
		for (int i = 0; i < testDataSize1; i++) {
			boolean[] kClassValues = new boolean[k];
			boolean testRowClass = false;
			double[] minKD = getKArray(k);
			int[] nearestKNeighbourIndex = new int[k];
			HashMap<String, String> curTestRow = data.get(testDataIndices
					.get(i));
			int exampleNum = testDataIndices.get(i) + 1;
			testRowClass = getBooleanValue(curTestRow.get("j"));
			/*
			 * System.out.println("finding " + k +
			 * " nearest neighbour for datarow " + exampleNum +
			 * " which has class value " + testRowClass);
			 */
			for (int j = 0; j < allDataSize; j++) {
				if (j == testDataIndices.get(i))
					continue;
				HashMap<String, String> dataRow2 = data.get(j);
				double curD = 0;
				for (String attName : curTestRow.keySet()) {
					if (attName.equals("j"))
						continue;
					String attType = attributesNameTypeMap.get(attName);
					switch (attType) {
					case "n":
						double partialD = deltaNumerical(
								Double.parseDouble(curTestRow.get(attName)),
								Double.parseDouble(dataRow2.get(attName)));
						curD += partialD * partialD;
						break;
					case "b":
						int partialDInt1 = deltaBinary(
								getBooleanValue(curTestRow.get(attName)),
								getBooleanValue(dataRow2.get(attName)));
						curD += (double) partialDInt1;
						break;
					case "c":
						int partialDInt2 = deltaCategorial(
								curTestRow.get(attName), dataRow2.get(attName));
						curD += (double) partialDInt2;
						break;
					}
				}
				curD = Math.sqrt(curD);
				updateMinD(minKD, nearestKNeighbourIndex, curD, j, kClassValues);

			}

			/*
			 * System.out.println("nearest " + k + " neighbour to example " +
			 * exampleNum + " is: " +
			 * getNearestNeighboursIndexString(nearestKNeighbourIndex) +
			 * " with minimum distance respectively " + getMinDistances(minKD) +
			 * " and class values " +
			 * getNearestNeighboursClassString(kClassValues)); System.out
			 * .println("-----------------------------------------------");
			 */
			// correctAnswer+=predictResult(kClassValues, testRowClass);
			predictResult(kClassValues, testRowClass);
		}
	}

	/**
	 * return 1 if the predicted result matches the true test result 0 otherwise
	 * 
	 * @param kClassValues
	 * @param testRowClass
	 * @return
	 */
	private void predictResult(boolean[] kClassValues, boolean testRowClass) {
		int positiveClasses = 0;
		for (int i = 0; i < kClassValues.length; i++) {
			if (kClassValues[i])
				positiveClasses++;
		}
		int negativeClasses = kClassValues.length - positiveClasses;
		if (negativeClasses > positiveClasses) {
			if (!testRowClass) {
				tn++;
				correctAnswer++;
			} else {
				fn++;
			}
		} else if (negativeClasses < positiveClasses) {
			if (testRowClass) {
				tp++;
				correctAnswer++;
			} else {
				fp++;
			}
		} // else if the number of negatives equals the number of positives,
			// randomly select
			// the classification
		else {
			int result = generateRandomNum();
			if (result == 1) {
				if (testRowClass) {
					tp++;
					correctAnswer++;
				} else
					fp++;
			} else {
				if (testRowClass)
					fn++;
				else {
					tn++;
					correctAnswer++;
				}

			}
		}
	}

	public int generateRandomNum() {
		Random rnd = new Random();
		return rnd.nextInt(2);
	}

	public String getNearestNeighboursIndexString(int[] nearestNeighbourIndices) {
		StringBuilder nearestIndices = new StringBuilder("{");
		for (int curIndex : nearestNeighbourIndices)
			nearestIndices.append(curIndex).append(",");
		nearestIndices.deleteCharAt(nearestIndices.length() - 1);
		return nearestIndices + "}";

	}

	public String getNearestNeighboursClassString(
			boolean[] nearestNeighbourClasses) {
		StringBuilder classValues = new StringBuilder("{");
		for (boolean curClass : nearestNeighbourClasses)
			classValues.append(curClass).append(",");
		classValues.deleteCharAt(classValues.length() - 1);
		return classValues + "}";

	}

	public String getMinDistances(double[] minDistances) {
		StringBuilder minDis = new StringBuilder("{");
		for (double curDis : minDistances)
			minDis.append(curDis).append(",");
		minDis.deleteCharAt(minDis.length() - 1);
		return minDis + "}";

	}

	private void updateMinD(double[] minKD, int[] nearestKNeighbourIndex,
			double curD, int newPotentialMminIndex, boolean[] kClassValue) {
		int maxIndex = getMaxIndex(minKD);
		if (curD <= minKD[maxIndex]) {
			minKD[maxIndex] = curD;
			nearestKNeighbourIndex[maxIndex] = newPotentialMminIndex;
			kClassValue[maxIndex] = getBooleanValue(data.get(
					newPotentialMminIndex).get("j"));
		}

	}

	private int getMaxIndex(double[] minKD) {
		double maxValue = minKD[0];
		for (int i = 1; i < minKD.length; i++)
			if (minKD[i] > maxValue)
				return i;
		return 0;
	}

	private double[] getKArray(int k) {
		double[] arr = new double[k];
		for (int i = 0; i < k; i++)
			arr[i] = 9999999;
		return arr;
	}

	private void generateRandom(int randomCount) {
		Random rnd = new Random();
		int count = 0;
		testDataIndices.clear();
		while (count < randomCount) {
			int curRndNum = rnd.nextInt(allDataSize);
			if (!(testDataIndices.contains(curRndNum))) {
				++count;
				testDataIndices.add(curRndNum);
			}
		}
	}

	public void parseData(String fileName) throws IOException {
		File file = new File(fileName);
		BufferedReader br = new BufferedReader(new FileReader(file));
		List<String> attributesList = new ArrayList<String>();

		// first line is list of attributes
		String row = br.readLine();
		String[] colTypes = row.split(",");
		for (String col : colTypes) {
			// attribute name consists of attrId:attrType
			String[] colId = col.split(":");
			attributesList.add(colId[0]);
			attributesNameTypeMap.put(colId[0], colId[1]);
		}
		while ((row = br.readLine()) != null) {
			HashMap<String, String> curRow = new HashMap<String, String>();
			String[] values = row.split(",");
			int i = 0;
			for (String curVal : values) {
				String attName = attributesList.get(i++);
				curRow.put(attName, curVal);
			}
			data.add(curRow);
		}
		br.close();
	}

	public int getFp() {
		return fp;
	}

	public int getTp() {
		return tp;
	}

	public void setTp(int tp) {
		this.tp = tp;
	}

	public int getFn() {
		return fn;
	}

	public int getTn() {
		return tn;
	}

}
