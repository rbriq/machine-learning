package decisionTree;

import java.util.*;

public class TDIDTUtil {

	public static boolean getBooleanValue(String string) {
		return string.equalsIgnoreCase("yes") ? true : false;
	}

	// methods for calculating gains for different types of attributes
	public static double calculateGainNumerical(double meanValue,
			List<DataClassPair<Double>> numericalData) {
		Map<String, Integer> classCountLarger = new HashMap<String, Integer>();
		Map<String, Integer> classCountSmaller = new HashMap<String, Integer>();
		double gain = 0;
		int totalCount = numericalData.size();
		for (DataClassPair<Double> curDataPair : numericalData) {
			String curClass = curDataPair.getClassValue();
			if (curDataPair.getValue() >= meanValue) {
				if(classCountLarger.get(curClass) == null)
					classCountLarger.put(curClass,0);
				classCountLarger.put(curClass,
						classCountLarger.get(curClass) + 1);
			} else {
				if(classCountSmaller.get(curClass) == null)
					classCountSmaller.put(curClass,0);
				classCountSmaller.put(curClass,
						classCountSmaller.get(curClass) + 1);
			}
		}

		Map<String, Integer> totalPerClass = new HashMap<String, Integer>();
		for (String curClass : classCountLarger.keySet()) {
			int largerCount = classCountLarger.get(curClass) == null ? 0
					: classCountLarger.get(curClass);
			int smallerCount = classCountSmaller.get(curClass) == null ? 0
					: classCountSmaller.get(curClass);
			int count = largerCount + smallerCount;
			totalPerClass.put(curClass, count);
		}
		for (String curClass : classCountSmaller.keySet()) {
			if (totalPerClass.get(curClass) == null)
				totalPerClass.put(curClass, classCountSmaller.get(curClass));
		}

		double totalEntropy = calculateHS(totalPerClass, totalCount);

		double partialEntropies = calculatePartialEntropies(classCountLarger,
				classCountSmaller);
		gain = totalEntropy - partialEntropies;
		//if(gain > 0.00001)
			//System.out.println("gain:" + gain);
		return gain;
	}

	public static double calculatePartialEntropies(
			Map<String, Integer> classCountLarger,
			Map<String, Integer> classCountSmaller
		//	Map<String, Integer> totalPerClass,
			) {

		int totalLargerCount = 0;
		int totalSmallerCount = 0;
		for (Integer curCount : classCountLarger.values()) {
			totalLargerCount += curCount;
		}
		for (Integer curCount : classCountSmaller.values()) {
			totalSmallerCount += curCount;
		}
		int totalCount = totalLargerCount + totalSmallerCount;

		double largerTotalEntory = 0;
		double smallerTotalEntory = 0;
		double largerCoeff = 0;
		double smallerCoeff = 0;
		largerCoeff = ((double) totalLargerCount / (double) totalCount);
		smallerCoeff = ((double) totalSmallerCount / (double) totalCount);
		for (String curClass : classCountLarger.keySet()) {
			int curTotal = classCountLarger.get(curClass);
			if (curTotal != 0) {
				int largerCountPerClass = classCountLarger.get(curClass) == null ? 0
						: classCountLarger.get(curClass);
				if(largerCountPerClass == 0)
					continue;
				largerTotalEntory += ((double) largerCountPerClass/ (double) totalLargerCount)
						* log2((double) totalLargerCount
								/ (double) largerCountPerClass);
			}
		}
		largerTotalEntory *= largerCoeff;

		for (String curClass : classCountSmaller.keySet()) {
			if(totalSmallerCount <=0 )
				break;
			int curTotal = classCountSmaller.get(curClass);
			if (curTotal != 0) {
				int smallerCountPerClass = classCountSmaller.get(curClass) == null ? 0
						: classCountSmaller.get(curClass);
				if(smallerCountPerClass == 0)
					continue;
				smallerTotalEntory += ((double) smallerCountPerClass/ (double) totalSmallerCount)
						* log2((double) totalSmallerCount
								/ (double) smallerCountPerClass);
			}
		}	
		smallerTotalEntory *= smallerCoeff;

		return largerTotalEntory + smallerTotalEntory;
	}

	public static double calculateHS(Map<String, Integer> totalPerClass, int totalCount) {

		double totalEntory = 0;
		for (String curClass : totalPerClass.keySet()) {

			int curTotal = totalPerClass.get(curClass);
			if (curTotal != 0) {
				totalEntory += ((double) curTotal / (double) totalCount)
						* log2((double) totalCount / (double) curTotal);
			}
		}
		return totalEntory;
	}

	public static double log2(double x) {
		// log2(x) = ln(x)/ln(2)
		return (Math.log(x) / Math.log(2));
	}

	public static String getSetStr(List<String> s) {
		StringBuilder str = new StringBuilder("{");
		for (String elem : s) {
			str.append(elem).append(",");
		}
		str.deleteCharAt(str.length() - 1);
		str.append("}");
		return str.toString();
	}

	// help classes
	public enum AttributeType {
		n, b, c, target
	};

	public static class DataClassPair<E> {

		private E value;
		private String classValue;

		public DataClassPair() {
		}

		public DataClassPair(E value, String classValue) {
			this.value = value;
			this.classValue = classValue;
		}

		public E getValue() {
			return value;
		}

		public void setValue(E value) {
			this.value = value;
		}

		public String getClassValue() {
			return classValue;
		}

		public void setClassValue(String classValue) {
			this.classValue = classValue;
		}

	}
}
