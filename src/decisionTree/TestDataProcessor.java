package decisionTree;

import java.util.List;
import java.util.Map;

import decisionTree.TDIDTUtil.AttributeType;
import decisionTree.TreeNode.NodeType;

public class TestDataProcessor {
	private static int correctAnswers = 0;
	private static String classId;
	
//	methods for checking answer
	public static void decideTestData(Map<String, List<String>> testDataSet,
			String passedClassId, TreeNode<?,?> decisionTree) {
		classId = passedClassId;
		int examplesCount = testDataSet.get(classId).size();
		for (int i = 0; i < examplesCount; i++) {
			System.out.println("Example " + i + " (" + 
					testDataSet.get(classId).get(i) + ") :");
			traverseTree(testDataSet, decisionTree, i);
			System.out.println();
		}
		System.out.println("Percent of correct answers " + correctAnswers + "/" +
				examplesCount + " = " + (double)correctAnswers/(double)examplesCount);
	}

	private static void traverseTree(Map<String, List<String>> testDataSet,
			TreeNode<?, ?> node, int rowIndex) {
		/*if (node.getNodeType() == NodeType.leaf) {
			if(TDIDTUtil.getBooleanValue(testDataSet.get(classId).get(rowIndex)) == 
					node.getClassValue())
				correctAnswers ++;
			if(node.getClassValue())
				System.out.print("class is 'yes'");
			else
				System.out.print("class is 'no'");
			return;
		}*/
		
		String curTestAtt = node.getTestAttribute();
		String curValue = testDataSet.get(curTestAtt).get(rowIndex);
		AttributeType curAttType = node.getAttributeType();
		System.out.print("Testing " + curTestAtt + ":" + curAttType);
		switch (curAttType) {
		case n:
			NumericalTreeNode numNode = (NumericalTreeNode) node;
			if (Double.parseDouble(curValue) > numNode.getSplitterValue()) {
				System.out.print(" i>" + numNode.getSplitterValue() + " -> ");
				traverseTree(testDataSet, numNode.getRightNode(), rowIndex);
			}
			else {
				System.out.print(" i<=" + numNode.getSplitterValue() + " -> ");
				traverseTree(testDataSet, numNode.getLeftNode(), rowIndex);
			}
			break;
		}
	}
}
