package decisionTree;

import decisionTree.TDIDTUtil.*;

/**
 * 
 * @author 
 *
 * @param <T> the test value type, for example, numerical, categorial.. 
 * @param <E> the splitter value, for example in case of numerical it is double, categorical - List<String>
 */
public class TreeNode <T, E>{

	public enum Direction {left, right}; 
	public enum NodeType {inner, leaf};
	
	
	protected TreeNode <?,?>leftNode;
	public TreeNode<?, ?> getLeftNode() {
		return leftNode;
	}
	public void setLeftNode(TreeNode<?,?> leftNode) {
		this.leftNode = leftNode;
	}

	protected TreeNode <?,?> rightNode;
	public TreeNode<?, ?> getRightNode() {
		return rightNode;
	}
	public void setRightNode(TreeNode<?, ?> rightNode) {
		this.rightNode = rightNode;
	}

//	by what answer we can get to this node
	protected String answer;
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String value) {
		this.answer = value;
	}
	
//	just number of a node
	protected int index;
	public int getIndex() {
		return index;
	}
	public void setIndex(int value) {
		this.index = value;
	}

//	what was the test - number for compare or subset to contain
	protected E splitterValue;
	public E getSplitterValue() {
		return splitterValue;
	}
	public void setSplitterValue(E splitterValue) {
		this.splitterValue = splitterValue;
	}
	
//	attribute type, i.e. numeric, boolean or categorical
	protected AttributeType attributeType;
	public AttributeType getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(AttributeType attributeType) {
		this.attributeType = attributeType;
	}

//	leaf or inner node
	protected NodeType nodeType = NodeType.inner;
	public NodeType getNodeType() {
		return nodeType;
	}
	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

//	i.e. result - final column, "yes" or "no"
	// this is relevant for leaf nodes
	protected boolean classValue ;
	public void setClassValue(boolean classValue) {
		this.classValue = classValue; 
	}
	public boolean getClassValue() {
		return classValue;
	}

//	id of the attribute in the node
	private String testAttribute;
	public void setTestAttribute(String testAttribute) {
		this.testAttribute = testAttribute;	
	}
	public String getTestAttribute() {
		return testAttribute;	
	}

	public TreeNode() {
	}
	
	public TreeNode(E splitterValue) {
		this.splitterValue = splitterValue;
	}

	public Direction decideDirection(T testValue){
		return Direction.right;
	}
}
 