package decisionTree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class InputDataProcessor {
	/**
	 * parse the data from the for processing while constructing the tree
	 * 
	 */
	public static void parseData(String fileName,
			List<String> attributesList,
			Map<String, List<String>> trainingDataSet,
			Map<String, List<String>> testDataSet) throws IOException {
		File file = new File(fileName);
		BufferedReader br = new BufferedReader(new FileReader(file));
//		store as map: attribute to values of that attribute
		//trainingDataSet = new LinkedHashMap<>();
//		first line is list of attributes
		String row = br.readLine();
		String[] colTypes = row.split(",");
		for (String col : colTypes) {
//			attribute name consists of attrId:attrType
			//String[] colId = col.split(":");
		
			attributesList.add(col);
			//attributeTypes.add(AttributeType.valueOf(colId[1]));
		}
//		store attribute name for the result
		String classId = attributesList.get(attributesList.size() - 1);
//		reading patterns
		while ((row = br.readLine()) != null) {
			String[] values = row.split(",");
			int i = 0;
			for (String curVal : values) {
				if(i==0) {
					++i;
					continue;
				}
				String key = attributesList.get(i);
				if (!trainingDataSet.containsKey(key)) {
					List<String> dataList = new ArrayList<String>();
					trainingDataSet.put(key, dataList);
				}
				trainingDataSet.get(key).add(curVal);
				++i;
			}
		}
		br.close();
		divideTrainingTest(attributesList, classId, testDataSet);
	}
	
	/**
	 * split the input data randomly into training data and test
	 * data
	 * 
	 */
	private static void divideTrainingTest(
			List<String> attributesList,
			String classId,
			Map<String, List<String>> testDataSet) throws IOException{
		File file = new File("C:/competition/test/test.csv");
		BufferedReader br = new BufferedReader(new FileReader(file));
//		store as map: attribute to values of that attribute
//		first line is list of attributes
		String row = br.readLine();
		
//		reading patterns
		while ((row = br.readLine()) != null) {
			String[] values = row.split(",");
			int i = 0;
			for (String curVal : values) {
				if(i==0) {
					++i;
					continue;
				}
				String key = attributesList.get(i);
				if (!testDataSet.containsKey(key)) {
					List<String> dataList = new ArrayList<String>();
					testDataSet.put(key, dataList);
				}
				testDataSet.get(key).add(curVal);
				++i;
			}
		}
		br.close();
		
	}
}
