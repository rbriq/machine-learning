package decisionTree;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class DecisionTreeDriver {

	public static void main(String[] args) throws IOException {
		TDIDTProcessor tditdProcessor = new TDIDTProcessor();
		tditdProcessor.constructDecisionTree(args[0]);
		File outputFile = new File(args[1]);
		FileOutputStream os = new FileOutputStream(outputFile);
		OutputTreeProcessor.printTree(tditdProcessor.getTree(), os);
		os.close();
		TestDataProcessor.decideTestData(tditdProcessor.getTestDataSet(),
				tditdProcessor.getClassId(),
				tditdProcessor.getTree());
	}

}
