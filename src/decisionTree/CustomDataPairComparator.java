package decisionTree;

import java.util.Comparator;

import decisionTree.TDIDTUtil.*;

// this class is used for sorting numeric attribute values
public class CustomDataPairComparator implements
	Comparator<DataClassPair<Double>> {
		
		@Override
		public int compare(DataClassPair<Double> object1,
			DataClassPair<Double> object2) {
		return object1.getValue().compareTo(object2.getValue());
	}
}