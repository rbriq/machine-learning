package decisionTree;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import decisionTree.TreeNode.NodeType;

public class OutputTreeProcessor {

	public static void printTree(TreeNode<?, ?> node, FileOutputStream os)
			throws IOException {
		if (node != null) {
			byte[] row;
			if (node.getNodeType() == NodeType.inner) {
				row = (node.getIndex() + " | " + node.getAnswer() + " | " + writeTest(node)).getBytes();
				os.write(row);
				if (node.getLeftNode() != null) {
					row = (" | " + String.valueOf(node.getLeftNode().getIndex()))
							.getBytes();
					os.write(row);
				}
				if (node.getRightNode() != null) {
					row = (" | " + node.getRightNode().getIndex()).getBytes();
					os.write(row);
				}
				row = "\n".getBytes();
				os.write(row);
				printTree(node.getLeftNode(), os);
				printTree(node.getRightNode(), os);
			}
			else {
				row = (node.getIndex() + " | " + node.getAnswer() + " | " + node.getClassValue() +" | _ | _").getBytes();
				os.write(row);
				row = "\n".getBytes();
				os.write(row);
			}
		}
	}
	
	private static String writeTest(TreeNode<?,?> node) {
		String formatted = "";
		switch(node.getAttributeType()) {
		case n:
			formatted = node.getTestAttribute() + " > " + node.getSplitterValue();
			break;
		case b:
			formatted = node.getTestAttribute();
			break;
		case c:
			formatted = node.getTestAttribute() + " in " + node.getSplitterValue();
			break;
		}
		return formatted;
	}
}
