package decisionTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import decisionTree.CustomDataPairComparator;
import decisionTree.TDIDTUtil.*;

public class NumericalTreeNode extends TreeNode<Double, Double> {

	public NumericalTreeNode(Double splitterValue) {
		super(splitterValue);
		attributeType = AttributeType.n;
	}

//	handles numerical attribute, i.e. finds the best split for comparing for the test
	public static double handleNumericalAtt(String attributeId, List<String> colData,
			String classId, Map<String, List<String>> trainingDataSet,
			Map<String, Double> attributeGain,
			Map<String, Double> numericalDataBestClassifier) {
		List<DataClassPair<Double>> numericalData = new ArrayList<DataClassPair<Double>>(
				colData.size());
		int i = 0;
//		build array of pairs - attribute value for the pattern and its class
		for (String curNum : colData) {
			String curClassValue = trainingDataSet.get(classId).get(i);
			DataClassPair<Double> newPair = new DataClassPair<Double>(
					Double.parseDouble(curNum), curClassValue);
			numericalData.add(newPair);
			++i;
		}
//		sorting pairs by attribute value
		Collections.sort(numericalData, new CustomDataPairComparator());
//		go through all the pairs and check for changing class value
//		and take mean value by algorithm
		Iterator<DataClassPair<Double>> iter = numericalData.iterator();
		DataClassPair<Double> prevDataPair = iter.next();
		double maxGain = 0;
		double bestClassifier = 0;
		while (iter.hasNext()) {
			DataClassPair<Double> nextDataPair = iter.next();
			if (prevDataPair.getClassValue() != nextDataPair.getClassValue()) {
				double meanValue = (prevDataPair.getValue()
						+ prevDataPair.getValue()) / 2.0;
				if(meanValue <0.0001)
					continue;
//				count gain for the changed class mean value
				double tempGain = TDIDTUtil.calculateGainNumerical(meanValue,
						numericalData);
				if (tempGain > maxGain) {
					maxGain = tempGain;
					bestClassifier = meanValue;
				}
			}
			prevDataPair = nextDataPair;
		}
		attributeGain.put(attributeId, maxGain);
		numericalDataBestClassifier.put(attributeId, bestClassifier);
		System.out.printf("Numeric: Got maximum gain %f for %s attribute comparing %f\n",
				maxGain, attributeId, bestClassifier);
		return maxGain;
	}

	@Override
	public Direction decideDirection(Double testValue) {
		if (testValue > splitterValue)
			return Direction.right;
		return Direction.left;
	}

}
