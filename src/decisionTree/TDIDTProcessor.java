package decisionTree;

import java.io.*;
import java.util.*;

import decisionTree.TDIDTUtil.*;
import decisionTree.TreeNode.*;

/**
 * The TDIT tree processor class responsible for building the decision tree
 *
 */
public class TDIDTProcessor {

	/**
	 * the rows from the input used as a training data for our decision tree key
	 * is the id of the column, and value is the column values
	 */
	private Map<String, List<String>> trainingDataSet = new LinkedHashMap<>();

	/**
	 * the rows from the input used as a test sample in our decision tree key is
	 * the id of the column, and value is the column values
	 */
	private Map<String, List<String>> testDataSet = new LinkedHashMap<>();
	public Map<String, List<String>> getTestDataSet() {
		return testDataSet;
	}
	/**
	 * The gain of each attribute key: column id value: the value of the gain
	 */
	private Map<String, Double> attributeGain = new HashMap<String, Double>();
	/**
	 * A map that holds that best mean values for the numerical attributes key:
	 * column Id value: the best mean value
	 */
	private Map<String, Double> numericalDataBestClassifier = new HashMap<String, Double>();
	private Map<String, List<String>> categorialDataBestSplit = new HashMap<String, List<String>>();

	private TreeNode<?, ?> rootNode;
	public TreeNode<?, ?> getTree() {
		return rootNode;
	}

	private int leftPositiveCount = 0;
	private int leftNegativeCount = 0;
	private int rightPositiveCount = 0;
	private int rightNegativeCount = 0;

	/**
	 * an ordered list of the attribute ids (a,b,c...j)
	 */
	private List<String> attributesList = new ArrayList<>();
	private List<String> usedAttributes = new ArrayList<>();
	/**
	 * an ordered list of column types(numerical, boolean,categorical or result)
	 */
	private List<AttributeType> attributeTypes = new ArrayList<>();
//	name of the result attribute
	private String classId;
	public String getClassId() {
		return classId;
	}
	
	private int indexNode = 1;

	/**
	 * This method is responsible for constructing the decision trees based on
	 * the input from the file
	 * 
	 */
	public void constructDecisionTree(String fileName) throws IOException {
//		all the params are out except for fileName
		InputDataProcessor.parseData(fileName, attributesList, trainingDataSet, testDataSet);
//		saving id of the result attribute
		classId = "target";
		rootNode = processTrainingData(trainingDataSet, attributesList, null);
	}

	/**
	 * main part of TDIDT algorithm
	 * process the training data using the algorithms described in the exercise.
	 * there are 3 types of attributes to handle: numerical, binary and
	 * categorical
	 * 
	 */
	private TreeNode<?,?> processTrainingData(
			Map<String, List<String>> trainingDataSetRecursive,
			List<String> attributesListRecursive,
			String parentAnswer) {
		TreeNode<?,?> node = null;
//		actually it won't ever happen - we do not take out numeric and categorical attributes
//		we already used all the attributes so cannot build another node
//		+1 - adding j attribute
		if (attributesListRecursive.size() == usedAttributes.size() + 1)
			return null;
		
		double maxGain = 0;
		String bestAttClassifier = "";
		AttributeType bestAttType = AttributeType.n;
		int i = 0;
//		iterate through the list of the attributes and count gain for each
		for (String curAttribute : trainingDataSetRecursive.keySet()) {
			if(usedAttributes.indexOf(curAttribute) < 0) {
				List<String> colData = trainingDataSetRecursive.get(curAttribute);
				AttributeType attType = AttributeType.n;
				double gain = 0;
				System.out.println("Attribute " + curAttribute + ":" + attType);
				if(curAttribute.equals("target"))
					continue;
				switch (attType) {
				case n:
					gain = NumericalTreeNode.handleNumericalAtt(curAttribute, colData,
							classId, trainingDataSetRecursive, 
							attributeGain, numericalDataBestClassifier);
					if (gain > maxGain) {
						maxGain = gain;
						bestAttClassifier = curAttribute;
						bestAttType = attType;
						node = new NumericalTreeNode(
								numericalDataBestClassifier.get(curAttribute));
					}
					break;
				
				case target:
					break;
				default:
					break;
				}
			}
			++i;
		}
//		remove boolean attribute that we used in node
		if(bestAttType == AttributeType.b) {
			usedAttributes.add(bestAttClassifier);
		}
//		System.out.println(usedAttributes);
		if(bestAttType != null) {
			System.out.println("--------Got maximum gain with " + bestAttClassifier + ":" + bestAttType);
			splitTestNode(bestAttClassifier, attributesListRecursive, trainingDataSetRecursive,
					node, bestAttType);
		}
		node.setIndex(indexNode++);
		node.setAnswer(parentAnswer);
		
		return node;
	}

//	splitting data set for child nodes
	private void splitTestNode(String bestAttClassifier,
			List<String> attributesListRecursive, Map<String, List<String>> trainingDataSetRecursive,
			TreeNode<?, ?> rootNodeRecursive, AttributeType bestAttType) {
		rootNodeRecursive.setTestAttribute(bestAttClassifier);
		switch (bestAttType) {
		case n:
			splitTestNumericNode(bestAttClassifier, attributesListRecursive, 
					trainingDataSetRecursive, rootNodeRecursive);
			break;
		case b:
			splitTestBinaryNode(bestAttClassifier, attributesListRecursive, 
					trainingDataSetRecursive, rootNodeRecursive);
			break;
		case c:
			splitTestCategorialNode(bestAttClassifier, attributesListRecursive,
					trainingDataSetRecursive, rootNodeRecursive);
			break;
		}
	}

//	splitting categorical node
	private void splitTestCategorialNode(String bestAttClassifier,
			List<String> attributesListRecursive,
			Map<String, List<String>> trainingDataSetRecursive, 
			TreeNode<?, ?> rootNodeRecursive) {
		Map<Direction, Map<String, List<String>>> leftRightValuesMap = sortTestCategNode(
				bestAttClassifier, trainingDataSetRecursive);
		Map<String, List<String>> leftExamples = leftRightValuesMap
				.get(Direction.left);
		Map<String, List<String>> rightExamples = leftRightValuesMap
				.get(Direction.right);

		buildChildren(rootNodeRecursive, attributesListRecursive,
				leftExamples, rightExamples);
	}

	private Map<Direction, Map<String, List<String>>> sortTestCategNode(
			String bestAttClassifier, Map<String, List<String>> trainingDataSet2) {
		Map<Direction, Map<String, List<String>>> leftRightValuesMap = new LinkedHashMap<>();
		leftRightValuesMap.put(Direction.left,
				new LinkedHashMap<String, List<String>>());
		leftRightValuesMap.put(Direction.right,
				new LinkedHashMap<String, List<String>>());

		leftPositiveCount = 0;
		leftNegativeCount = 0;
		rightPositiveCount = 0;
		rightNegativeCount = 0;
		
		int i = 0;
		List<String> bestSet = categorialDataBestSplit.get(bestAttClassifier);
//		right direction is for true branch, left for false
		for (String curValue : trainingDataSet2.get(bestAttClassifier)) {
			if (bestSet.contains(curValue))
				addRow(leftRightValuesMap.get(Direction.right),
						trainingDataSet2, i++, Direction.right);
			else
				addRow(leftRightValuesMap.get(Direction.left),
						trainingDataSet2, i++, Direction.left);
		}
		
		return leftRightValuesMap;
	}

//	splitting binary node
	private void splitTestBinaryNode(String bestAttClassifier,
			List<String> attributesListRecursive,
			Map<String, List<String>> trainingDataSetRecursive,
			TreeNode<?, ?> rootNodeRecursive) {
		Map<Direction, Map<String, List<String>>> leftRightValuesMap = sortTestBinaryNode(
				bestAttClassifier, trainingDataSetRecursive);
		Map<String, List<String>> leftExamples = leftRightValuesMap
				.get(Direction.left);
		Map<String, List<String>> rightExamples = leftRightValuesMap
				.get(Direction.right);

		buildChildren(rootNodeRecursive, attributesListRecursive,
				leftExamples, rightExamples);
	}

	private Map<Direction, Map<String, List<String>>> sortTestBinaryNode(
			String bestAttClassifier, Map<String, List<String>> trainingDataSet2) {
		Map<Direction, Map<String, List<String>>> leftRightValuesMap = new LinkedHashMap<>();
		leftRightValuesMap.put(Direction.left,
				new LinkedHashMap<String, List<String>>());
		leftRightValuesMap.put(Direction.right,
				new LinkedHashMap<String, List<String>>());

		leftPositiveCount = 0;
		leftNegativeCount = 0;
		rightPositiveCount = 0;
		rightNegativeCount = 0;
		
		int i = 0;
		for (String curValue : trainingDataSet2.get(bestAttClassifier)) {
//			right direction is for true branch, left for false
			boolean curValueB = TDIDTUtil.getBooleanValue(curValue);
			if (curValueB)
				addRow(leftRightValuesMap.get(Direction.right),
						trainingDataSet2, i++, Direction.right);
			else
				addRow(leftRightValuesMap.get(Direction.left),
						trainingDataSet2, i++, Direction.left);
		}
		return leftRightValuesMap;
	}

//	splitting numeric node
	private void splitTestNumericNode(String bestAttClassifier,
			List<String> attributesListRecursive,
			Map<String, List<String>> trainingDataSetRecursive,
			TreeNode<?, ?> rootNodeRecursive) {
		Map<Direction, Map<String, List<String>>> leftRightValuesMap = sortTestNumericNode(
				bestAttClassifier, trainingDataSetRecursive);
		Map<String, List<String>> leftExamples = leftRightValuesMap
				.get(Direction.left);
		Map<String, List<String>> rightExamples = leftRightValuesMap
				.get(Direction.right);

		buildChildren(rootNodeRecursive, attributesListRecursive,
				leftExamples, rightExamples);
	}

	private Map<Direction, Map<String, List<String>>> sortTestNumericNode(
			String bestAttClassifier, Map<String, List<String>> trainingDataSet2) {
		Map<Direction, Map<String, List<String>>> leftRightValuesMap = new LinkedHashMap<>();
		leftRightValuesMap.put(Direction.left,
				new LinkedHashMap<String, List<String>>());
		leftRightValuesMap.put(Direction.right,
				new LinkedHashMap<String, List<String>>());
		
		leftPositiveCount = 0;
		leftNegativeCount = 0;
		rightPositiveCount = 0;
		rightNegativeCount = 0;
		
		double classifierValue = numericalDataBestClassifier
				.get(bestAttClassifier);
		int i = 0;
		for (String curValue : trainingDataSet2.get(bestAttClassifier)) {
//			right direction is for true branch, left for false
			Double curValueD = Double.parseDouble(curValue);
			if (curValueD > classifierValue)
				addRow(leftRightValuesMap.get(Direction.right),
						trainingDataSet2, i, Direction.right);
			else
				addRow(leftRightValuesMap.get(Direction.left),
						trainingDataSet2, i, Direction.left);
			++i;
		}
		return leftRightValuesMap;
	}

//	add row to child data set accordingly to the test
	private void addRow(Map<String, List<String>> map,
			Map<String, List<String>> trainingDataSet2, int rowIndex,
			Direction dir) {

		for (int i = 0; i < attributesList.size(); i++) {
			String att = attributesList.get(i);
			if(!map.containsKey(att)) {
				map.put(att, new ArrayList<String>());
			}
			map.get(att).add(trainingDataSet2.get(att).get(rowIndex));
			boolean classValue = TDIDTUtil.getBooleanValue(trainingDataSet2
					.get(classId).get(rowIndex));
			if (dir == Direction.right) {
				if (classValue)
					++rightPositiveCount;
				else
					++rightNegativeCount;
			}
			if (dir == Direction.left) {
				if (classValue)
					++leftPositiveCount;
				else
					++leftNegativeCount;
			}
		}
	}
	
//	build child nodes
	private void buildChildren(TreeNode<?,?> rootNodeRecursive, 
			List<String> attributesListRecursive,
			Map<String, List<String>> leftExamples,
			Map<String, List<String>> rightExamples) {
		TreeNode<?, ?> leftNode = new TreeNode();
		TreeNode<?, ?> rightNode = new TreeNode();
//		check if we have "perfect" split
		if (leftNegativeCount != 0 && leftPositiveCount != 0)
//			build the node if we do not have leaf
			rootNodeRecursive.setLeftNode(processTrainingData(leftExamples, 
					attributesListRecursive, "no"));
		else {
			leftNode.setNodeType(NodeType.leaf);
			leftNode.setClassValue(leftPositiveCount > 0);
			leftNode.setIndex(indexNode++);
			leftNode.setAnswer("no");
			rootNodeRecursive.setLeftNode(leftNode);
		}

//		either check if we have "perfect" split
		if (rightNegativeCount != 0 && rightPositiveCount != 0)
//			build the node if we do not have leaf
			rootNodeRecursive.setRightNode(processTrainingData(rightExamples, 
					attributesListRecursive, "yes"));
		else {
			rightNode.setNodeType(NodeType.leaf);
			rightNode.setClassValue(rightPositiveCount > 0);
			rightNode.setIndex(indexNode++);
			rightNode.setAnswer("yes");
			rootNodeRecursive.setRightNode(rightNode);
		}
	}
}
